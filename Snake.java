public class Snake extends Animal {

    public Snake() {
        this.size = 5;
    }

    @Override
    public String toString() {
        return "Snake{" +
                "size=" + size +
                '}';
    }
}
